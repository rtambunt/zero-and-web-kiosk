import { Navbar } from "./components/Navbar";
import { Category } from "./components/categories/Category";
import { Header } from "./components/Header";
import { ShoppingCart } from "./components/checkout/ShoppingCart";
import { useShoppingCart } from "./components/context/ShoppingCartContext";
import { AiOutlineShoppingCart } from "react-icons/ai";

function App() {
  const { isOpen, openCart } = useShoppingCart();
  return (
    <>
      <Header />
      <div className="bg-gray-100 h-screen flex items-start relative">
        <div className="mx-auto px-10 w-9/12 ">
          <Navbar />
          <Category />
        </div>
        {isOpen && <ShoppingCart />}
        {!isOpen && (
          <button
            onClick={openCart}
            className="absolute bottom-44 right-16 p-5 border-2 rounded-full text-gray-200 bg-gray-900 flex"
          >
            <AiOutlineShoppingCart size={35} />
          </button>
        )}
      </div>
    </>
  );
}

export default App;
