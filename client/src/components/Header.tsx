import React from "react";
import { AiFillClockCircle } from "react-icons/ai";

export const Header = () => {
  return (
    <div className="flex items-center h-28">
      <a href="#">
        <img
          src="https://web.zeroand.com/order/img/logo.png"
          alt=""
          className="w-28 px-6 py-5"
        />
      </a>
      <div>
        <div className="text-md font-semibold">ZERO& Stoneridge Mall Store</div>
        <div className="flex items-center mt-1">
          <AiFillClockCircle size={13} />
          <div className="text-sm font-medium ml-1 ">
            Hours: 10:00AM - 8:00PM
          </div>
        </div>
      </div>
    </div>
  );
};
