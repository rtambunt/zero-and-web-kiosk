import { IconContext } from "react-icons";
import { AiFillStar } from "react-icons/ai";
import {
  TbApple,
  TbMilk,
  TbPlant,
  TbBread,
  TbCake,
  TbShirt,
  TbGift,
} from "react-icons/tb";
import { CgCoffee } from "react-icons/cg";
import { MdOutlineCoffeeMaker } from "react-icons/md";
import { BsBag } from "react-icons/bs";

type NavItem = {
  name: string;
  icon: JSX.Element;
};

export const Navbar = () => {
  const navItems: NavItem[] = [
    { name: "New or Seasonal", icon: <AiFillStar /> },
    { name: "Fruit Tea", icon: <TbApple /> },
    { name: "Milk Tea", icon: <TbMilk /> },
    { name: "Pure Tea", icon: <TbPlant /> },
    { name: "Hot Drinks", icon: <CgCoffee /> },
    { name: "Coffee", icon: <MdOutlineCoffeeMaker /> },
    { name: "Hanabi Bread", icon: <TbBread /> },
    { name: "Hanabi Dessert", icon: <TbCake /> },
    { name: "Swags", icon: <TbShirt /> },
    { name: "Gift Card", icon: <TbGift /> },
    { name: "Bag", icon: <BsBag /> },
  ];

  return (
    <div className="flex bg-gray-200 whitespace-nowrap overflow-auto no-scrollbar">
      <IconContext.Provider value={{ size: "1rem" }}>
        {navItems.map((navItem, index) => (
          <a
            className="px-5 py-3 active:bg-slate-50 rounded-t-md hover:bg-slate-50 transition-all duration-300"
            href="#"
            key={`${navItem.name} ${index}`}
          >
            <div className="flex items-center justify-center">
              {navItem.icon}
              <div className="ml-2">{navItem.name}</div>
            </div>
          </a>
        ))}
      </IconContext.Provider>
    </div>
  );
};
