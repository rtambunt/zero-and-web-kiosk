import React from "react";
import { newData } from "../../data/data";
import { Item } from "./Item";
import type { Drink, NonDrink } from "../../data/data";

export const Category = () => {
  return (
    <div className="mt-3">
      <div className="font-semibold text-xl mb-5">New or Seasonal</div>
      <div className="grid grid-cols-auto-fit auto-rows gap-x-8 gap-y-7">
        {newData.map((itemData, index) => (
          <Item itemData={itemData} key={`${itemData} ${index}`} />
        ))}
      </div>
    </div>
  );
};
