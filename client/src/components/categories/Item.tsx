import React from "react";
import type { Drink, NonDrink } from "../../data/data";
import { BsFillArrowRightCircleFill } from "react-icons/bs";
import { AiFillPlusCircle } from "react-icons/ai";
import { useShoppingCart } from "../context/ShoppingCartContext";

type ItemProps = {
  itemData: Drink | NonDrink;
};

export const Item = ({ itemData }: ItemProps) => {
  const { cartItems, addItem } = useShoppingCart();
  return (
    <a href="#" className="block">
      <div className="flex bg-white rounded-lg h-full pr-10 pl-7 py-5 relative">
        <img className="w-28 h-28" src={itemData.imgUrl} />
        <div className="ml-4 flex flex-col">
          <div className="mb-2 text-lg font-medium leading-6">
            {itemData.name}
          </div>
          <div className=" h-8 mb-10 text-2xs leading-4 text-slate-500 overflow-hidden">
            {itemData.description}
          </div>
          <div className="absolute bottom-4 flex items-center">
            <div>
              <div className="font-semibold inline-block text-sm">
                ${itemData.price}
              </div>
              {"size" in itemData && (
                <div className="ml-3 px-1 inline-block border-2 border-solid border-gray-500 rounded-2xl text-2xs">
                  {itemData.size}oz
                </div>
              )}
            </div>
          </div>
          <div className="absolute bottom-4 right-4">
            <button
              onClick={() => {
                addItem(itemData.id);
              }}
            >
              <AiFillPlusCircle size={22} />
            </button>

            {/* <BsFillArrowRightCircleFill size={22} /> */}
          </div>
        </div>
      </div>
    </a>
  );
};
