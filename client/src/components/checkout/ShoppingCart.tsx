import React from "react";
import { useShoppingCart } from "../context/ShoppingCartContext";
import { ShoppingCartItem } from "./ShoppingCartItem";
import { IoClose } from "react-icons/io5";

export const ShoppingCart = () => {
  const { closeCart, cartItems } = useShoppingCart();
  return (
    <div className="flex flex-col justify-between relative bg-slate-50 h-[calc(100vh-var(--header-height))] w-full border-2 border-gray-300">
      <button
        onClick={closeCart}
        className="absolute top-2 right-2 text-gray-500"
      >
        <IoClose size={32} />
      </button>
      <div className="px-10 pt-8">
        <div className="text-2xl font-semibold">Your Order</div>
      </div>
      <div className="h-full my-5 px-10">
        {cartItems.map((item) => (
          <ShoppingCartItem item={item} key={item.id} />
        ))}
      </div>
      <a href="#" className="bg-gray-900 text-white w-full pt-5 pb-7 px-7">
        Continue to Checkout
      </a>
    </div>
  );
};
