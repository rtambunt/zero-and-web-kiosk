import React from "react";
import { useShoppingCart, CartItem } from "../context/ShoppingCartContext";
import { newData } from "../../data/data";
import { Item } from "../categories/Item";

type ShoppingCartItemProps = {
  item: CartItem;
};

export const ShoppingCartItem = ({ item }: ShoppingCartItemProps) => {
  const { removeItem } = useShoppingCart();
  const id = item.id;
  const thisItem = newData.find((curItem) => id === curItem.id);
  return (
    <div>
      <div className="flex justify-between">
        <div>{item.quantity}</div>
        <div>{thisItem?.name}</div>
        <div>${thisItem?.price}</div>
      </div>
      <a onClick={() => removeItem(id)} href="#">
        remove
      </a>
    </div>
  );
};
