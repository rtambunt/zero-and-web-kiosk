import React, { ReactNode, createContext, useContext, useState } from "react";
import { newData } from "../../data/data";

export type CartItem = {
  id: number;
  quantity: number;
};

type ShoppingCartProviderProps = {
  children: ReactNode;
};

type ShoppingCartContext = {
  cartItems: CartItem[];
  isOpen: boolean;
  openCart: () => void;
  closeCart: () => void;
  getItemQuantity: (id: number) => number;
  addItem: (id: number) => void;
  removeItem: (id: number) => void;
};

const ShoppingCartContext = createContext({} as ShoppingCartContext);

// Custom hook to specify shopping cart context
export const useShoppingCart = () => useContext(ShoppingCartContext);

export const ShoppingCartProvider = ({
  children,
}: ShoppingCartProviderProps) => {
  const [cartItems, setCartItems] = useState<CartItem[]>([]);
  const [isOpen, setIsOpen] = useState(false);

  const openCart = () => setIsOpen(true);
  const closeCart = () => setIsOpen(false);

  const getItemQuantity = (id: number) => {
    return cartItems.find((item) => item.id === id)?.quantity || 0;
  };

  const addItem = (id: number) => {
    setCartItems((curItems) => {
      if (!curItems.find((item) => item.id === id)) {
        openCart();
        return [...curItems, { id, quantity: 1 }];
      } else {
        return curItems.map((item) => {
          if (item.id === id) {
            return { ...item, quantity: item.quantity + 1 };
          } else {
            return item;
          }
        });
      }
    });
  };

  const removeItem = (id: number) => {
    setCartItems(cartItems.filter((item) => item.id !== id));
    cartItems.length <= 1 && closeCart();
  };

  // const removeItem = (id: number) => {
  //   setCartItems((curItems) => {
  //     return curItems.map((item) => {
  //       if (item.id === id && item.quantity > 0) {
  //         return { ...item, quantity: 0 };
  //       } else {
  //         return item;
  //       }
  //     });
  //   });

  //   cartItems.length <= 0 && closeCart();
  // };

  return (
    <ShoppingCartContext.Provider
      value={{
        isOpen,
        openCart,
        closeCart,
        cartItems,
        getItemQuantity,
        addItem,
        removeItem,
      }}
    >
      {children}
    </ShoppingCartContext.Provider>
  );
};
