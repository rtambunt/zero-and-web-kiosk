export type Drink = {
  id: number;
  name: string;
  description: string;
  imgUrl: string;
  price: number;
  size: number;
};

export type NonDrink = {
  id: number;
  name: string;
  description: string;
  imgUrl: string;
  price: number;
};

// export const isDrink = (item: Drink | NonDrink): item is Drink => {
//   return;
// };

export const newData: (Drink | NonDrink)[] = [
  {
    id: 0,
    name: "Thai Me Up",
    description: "Thai tea, Crème Brûlée, Crushed Oreos",
    imgUrl:
      "https://298383278759-fuji-prod-us-west-2.s3-accelerate.amazonaws.com/fuji-product-webapp/202303/df74c8cd-fad0-4826-941d-2a8a0417d4be.jpg",
    price: 5.45,
    size: 16,
  },
  {
    id: 1,
    name: "Hot Thai Me Up",
    description: "Thai tea, Crème Brûlée, Crushed Oreos",
    imgUrl:
      "https://298383278759-fuji-prod-us-west-2.s3-accelerate.amazonaws.com/fuji-product-webapp/202303/d8685e22-642b-453d-a6a2-4a21ab220660.jpg",
    price: 5.45,
    size: 16,
  },
  {
    id: 2,
    name: "Peach Blossom Mini Box",
    description:
      "Cream Cheese Lemon Chiffon Cake; Handcrafted Peach Confiture; Cream Cheese Vanilla Chantilly Cream, Peach Chantilly Cream; White Chocolate Crunchy Pearls",
    imgUrl:
      "https://298383278759-fuji-prod-us-west-2.s3-accelerate.amazonaws.com/fuji-product-webapp/202304/208d3b7d-f85e-4825-b53b-04bf97d8e327.jpg",
    price: 8.95,
  },
  {
    id: 3,
    name: "Rose Lychee Dream Mini Box - Gluten Free",
    description:
      "Gluten-Free Vanilla Chiffon; Light Rose Chantilly Cream; Handcrafted Lychee Coulis; Edible Rose Bud and Petals",
    imgUrl:
      "https://298383278759-fuji-prod-us-west-2.s3-accelerate.amazonaws.com/fuji-product-webapp/202303/124fb75e-6859-49b6-b3b6-b4879f518f42.jpg",
    price: 8.95,
  },
];
