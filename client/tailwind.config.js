/** @type {import('tailwindcss').Config} */
const plugin = require("tailwindcss/plugin");

export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {},
    fontFamily: {
      sans: ["Inter", "Avenir", "Helvetica", "sans-serif"],
      serif: ["Merriweather", "serif"],
    },
    fontSize: {
      "2xs": ".7rem",
      "3xs": ".5rem",
      sm: "0.8rem",
      md: "1rem",
      lg: "1.17rem",
      xl: "1.25rem",
      "2xl": "1.563rem",
      "3xl": "1.953rem",
      "4xl": "2.441rem",
      "5xl": "3.052rem",
    },
    gridTemplateColumns: {
      // auto-fit css
      "auto-fit": "repeat(auto-fit, minmax(350px, 1fr))",
    },
  },
  plugins: [
    plugin(function ({ addUtilities }) {
      addUtilities({
        ".no-scrollbar::-webkit-scrollbar": {
          display: "none",
        },
      });
    }),
  ],
};
